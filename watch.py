import serial
import time

outbuf = []
for i in range(32):
  outbuf.append(b'\x00')

resetmsg=outbuf.copy()
resetmsg[0]=b'\xff'
resetmsg[1]=b'\xff'
resetmsg=b''.join(resetmsg)

snmsg=outbuf.copy()
snmsg[0]=b'\xee'
snmsg[1]=b'\xee'
snmsg=b''.join(snmsg)

#testmsg=outbuf.copy()
#testmsg[0]=b'\xcc'
#testmsg[1]=b'\xcc'
#testmsg=b''.join(testmsg)

ifc = serial.Serial('/dev/tty.SLAB_USBtoUART',baudrate=115200, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, timeout=1, xonxoff=False, rtscts=False, write_timeout=None, dsrdtr=False, inter_byte_timeout=None, exclusive=None)

#print('resetting')
#ifc.write(resetmsg)
#time.sleep(.005)
#ifc.flush()
#time.sleep(.005)
#print('requesting sn')
#ifc.write(snmsg)
#print(ifc.read(36))

# pseudocode

#loop
i = 0
while (i < 255):
    testmsg=outbuf.copy()
    #testmsg[0]=b'\x00'
    #testmsg[1]=b'\x00'
    testmsg[0]=b'\x08'
    testmsg[1]=b'\x08'
    testmsg[2]=i.to_bytes(1,'big')
    testmsg[3]=testmsg[2]
    testmsg[2]=b'\x0a'
    testmsg[3]=b'\x0a'
    testmsg=b''.join(testmsg)
# send a reset \xff \xff
    ifc.write(resetmsg)
    #time.sleep(.005)
    #ifc.flush()
    #time.sleep(.005)
    ifc.write(snmsg)
    print('reading SN' + str(ifc.read(36)))
    #time.sleep(.005)
    #ifc.flush()
    #time.sleep(.005)
# send a command*
    ifc.write(testmsg)
    #time.sleep(.005)
    #ifc.flush()
    #time.sleep(.005)
    print('writing ' + str(testmsg))
# read (if anything) and print to screen
    print(ifc.read(36))
# if anything, write to disk log
# next loop
    i = i + 1
    ifc.write(resetmsg)
    time.sleep(.5)
    ifc.flush()
    print()

# * command is \x00 \x00 -> incremented to \xfe \xfe
# * \xee \xee should produce a result

exit()

buf = outbuf.copy()
buf[0]=b'\xff'
val=ord(buf[0])
val = val - 1
buf[0] = val.to_bytes(1,'big')
buf[1]=b'\xff'
bufstream = b''.join(buf)
print(bufstream)



time.sleep(10)

#ifc.write(resetmsg)
#time.sleep(.005)
#ifc.flush()
#time.sleep(.005)
ifc.write(testmsg)
print(ifc.read(36))

ifc.write(resetmsg)



